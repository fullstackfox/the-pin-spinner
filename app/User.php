<?php

namespace PinSpinner;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pins()
    {
        return $this->hasMany(Pin::class);
    }

    public function boards()
    {
        return $this->hasMany(Board::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'title');
    }
}
