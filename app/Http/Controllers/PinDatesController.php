<?php

namespace PinSpinner\Http\Controllers;

use PinSpinner\Pin;
use PinSpinner\Board;
use PinSpinner\PinDate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PinDatesController extends Controller
{
    public function store(Board $board, Pin $pin)
    {
        $pinDate = new PinDate();
        $time = new Carbon();

        $pinDate->pin_id = $pin->id;
        $pinDate->board_id = $board->id;
        $pinDate->pinned_at = $time->timestamp;

        $pinDate->save();
    }
}
