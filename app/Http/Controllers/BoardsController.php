<?php

namespace PinSpinner\Http\Controllers;

use Carbon\Carbon;
use PinSpinner\Pin;
use PinSpinner\Board;
use PinSpinner\PinDate;
use PinSpinner\Category;
use Illuminate\Http\Request;
use PinSpinner\Http\Requests\StoreBoard;

class BoardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boards = Board::where('user_id', auth()->id())->get();
        $categories = Category::where('user_id', '=', auth()->id())->get();

        return view('boards.index', compact('boards', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('user_id', '=', auth()->id())->get();

        if ($categories->count() == 0) {
            return redirect()->route('home');
        }

        return view('boards.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(StoreBoard $request)
    {
        $board = new Board();

        $board->user_id = auth()->id();
        $board->category_title = $request->category_title;
        $board->title = $request->title;
        $board->link = $request->link;
        $board->save();

        if (request()->expectsJson()) {
            return $board;
        }

        return redirect()->route('board.index');
    }

    /**
     * Display the specified resource.
     *
     * @param $board
     * @return \Illuminate\Http\Response
     */
    public function show($board)
    {
        $pins = Board::findOrFail($board)->getAvailablePins($board);

        return view('boards.show', compact('pins'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setPin(Board $board, Pin $pin)
    {
        $setPin = new PinDate();
        $time = new Carbon();

        $setPin->pin_id = $pin->id;
        $setPin->board_id = $board->id;
        $setPin->pinned_at = $time->timestamp;

        $setPin->save();
    }
}
