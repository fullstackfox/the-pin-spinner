<?php

namespace PinSpinner;

use Illuminate\Database\Eloquent\Model;

class PinDate extends Model
{
    protected $table = 'pin_dates';
    protected $guarded = [];
}
