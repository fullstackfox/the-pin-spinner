<?php

namespace PinSpinner;

use PinSpinner\Board;
use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function boards()
    {
        return $this->belongsToMany(Board::class);
    }
}
