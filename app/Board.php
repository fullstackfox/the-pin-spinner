<?php

namespace PinSpinner;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pins()
    {
        return $this->belongsToMany(Pin::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'title');
    }

    public function getAvailablePins($board)
    {
        $board = Board::where('user_id', auth()->id())
                      ->findOrFail($board);

        if ($board->count == 0) {
            return false;
        } else {
            $pins = $board->pins()
                    ->whereNotIn('pins.id', PinDate::pluck('pin_id')
                    ->all())->get();
        }

        return $pins;
    }
}
