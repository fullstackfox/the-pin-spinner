<?php

use PinSpinner\Category;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('pin', 'PinsController');
Route::resource('board', 'BoardsController');

Route::get('/category', function () {
    $categories = Category::where('user_id', auth()->id())->get();

    return $categories;
});

Route::post('/pin-date/{board}/{pin}', 'PinDatesController@store')->name('pin-date.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
