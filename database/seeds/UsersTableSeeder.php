<?php

use PinSpinner\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Logan',
            'email' => 'logan@logan.com',
            'password' => bcrypt('dashboard'),
        ]);
    }
}
