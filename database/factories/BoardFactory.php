<?php

use PinSpinner\Board;
use Faker\Generator as Faker;

$factory->define(Board::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('PinSpinner\User')->create()->id;
        },
        'category_title' => function () {
            return factory('PinSpinner\Category')->create()->title;
        },
        'title' => $faker->sentence(20),
        'link' => $faker->url,
    ];
});
