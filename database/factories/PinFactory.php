<?php

use PinSpinner\Link;
use PinSpinner\Pin;
use Faker\Generator as Faker;

$factory->define(Pin::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('PinSpinner\User')->create()->id;
        },
        'title' => $faker->sentence(20),
        'link' => $faker->url,
    ];
});