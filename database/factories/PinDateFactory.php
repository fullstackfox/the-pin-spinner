<?php

use Faker\Generator as Faker;

$factory->define(\PinSpinner\PinDate::class, function (Faker $faker) {
    return [
        'pin_id' => function () {
            return factory(\PinSpinner\Pin::class)->create()->id;
        },
        'board_id' => function () {
            return factory(\PinSpinner\Board::class)->create()->id;
        },
        'pinned_at' => $faker->dateTime
    ];
});
