<?php

use PinSpinner\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('PinSpinner\User')->create()->id;
        },
        'title' => $faker->unique()->word
    ];
});
