<?php

namespace Tests\Feature;

use PinSpinner\Board;
use PinSpinner\Category;
use PinSpinner\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateBoardsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_unauthorized_user_cannot_create_a_board()
    {
        $this->withExceptionHandling();

        $this->get(route('board.create'))
            ->assertRedirect('/login');

        $this->post(route('board.store'))
            ->assertRedirect('/login');
    }

    /** @test */
    function an_authorized_user_can_create_a_board()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(create(User::class));

        $category = create(Category::class);
        $board = make(Board::class, ['category_title' => $category->title]);

        $response = $this->post(route('board.store'), $board->toArray());

        $this->assertDatabaseHas('boards', [
            'user_id' => auth()->id(),
            'category_title' => $category->title,
            'title' => $board->title,
            'link' => $board->link
        ]);
    }

    /** @test */
    public function a_board_requires_a_category()
    {
        $this->publishBoard(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_board_requires_a_title()
    {
        $this->publishBoard(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_board_requires_a_link()
    {
        $this->publishBoard(['link' => null])
            ->assertSessionHasErrors('link');
    }

    public function publishBoard($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $board = make(Board::class, $overrides);

        return $this->post(route('board.store'), $board->toArray());
    }
}
