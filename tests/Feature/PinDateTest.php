<?php

namespace Tests\Unit;

use Tests\TestCase;
use PinSpinner\Pin;
use PinSpinner\User;
use PinSpinner\Board;
use PinSpinner\PinDate;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PinDateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function store_pin_date()
    {
        $this->actingAs(create(User::class));

        $board = create(Board::class);
        $pin = create(Pin::class);

        $response = $this->post("/pin-date/{$board->id}/{$pin->id}");

        $this->assertDatabaseHas('pin_dates', [
            'pin_id' => $pin->id,
            'board_id' => $board->id
        ]);
    }
}
