<?php

namespace Tests\Feature;

use PinSpinner\Pin;
use PinSpinner\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePinsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_unauthorized_user_cannot_create_a_pin()
    {
        $this->withExceptionHandling();

        $this->get(route('pin.create'))
            ->assertRedirect('/login');

        $this->post(route('pin.store'))
            ->assertRedirect('/login');
    }

    /** @test */
    function an_authorized_user_can_create_a_pin()
    {
        $this->actingAs(create(User::class));

        $pin = make(Pin::class);

        $response = $this->post(route('pin.store'), $pin->toArray());

        $this->assertDatabaseHas('pins', [
            'user_id' => auth()->id(),
            'title' => $pin->title,
            'link' => $pin->link
        ]);
    }

    /** @test */
    public function a_pin_requires_a_title()
    {
        $this->publishPin(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_pin_requires_a_link()
    {
        $this->publishPin(['link' => null])
            ->assertSessionHasErrors('link');
    }

    public function publishPin($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $pin = make(Pin::class, $overrides);

        return $this->post(route('pin.store'), $pin->toArray());
    }
}
