@extends('layouts.app')

@section('content')
<div class="mx-w-full flex justify-center">
    <div class="w-full max-w-xs mt-16">
        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
            @csrf

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="name">
                    {{ __('Name') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" name="name" value="{{ old('name') }}" required autofocus id="name" type="text" placeholder="Name">

                @if ($errors->has('name'))
                    <div class="bg-red-lighter p-2 my-3 rounded" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                @endif
            </div>

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                    {{ __('E-Mail Address') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <div class="bg-red-lighter p-2 my-3 rounded" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
            </div>

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    {{ __('Password') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Password" name="password" required>

                @if ($errors->has('password'))
                    <div class="bg-red-lighter p-2 my-3 rounded" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                @endif
            </div>

            <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                    {{ __('Confirm Password') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 mb-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="password-confirm" type="password" placeholder="Password" name="password_confirmation" required>
            </div>

            <div class="flex justify-center">
                <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    {{ __('Register') }}
                </button>
            </div>

        </form>

        <p class="text-center text-grey text-xs">
            ©2018 The Pin Spinner. All rights reserved.
        </p>
    </div>
</div>
@endsection
