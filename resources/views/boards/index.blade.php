@extends('layouts.app')

@section('content')
    <!-- Three columns -->
    <div>
        <boards :data="{{ $boards }}"></boards>
    </div>
@stop