<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body class="bg-grey-lighter">
    <div>
        @include('layouts.navbar')

        <div id="app" class="container mx-auto py-12">
            {{--@include('flash::message')--}}

            @yield('content')
        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
