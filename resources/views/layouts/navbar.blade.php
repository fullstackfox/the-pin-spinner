<nav class="bg-blue">
    <div class="container mx-auto py-6 flex">

        <h1 class="flex-1"><a href="" class="no-underline text-white hover:text-grey-light">ThePinSpinner</a></h1>

        <div class="flex-1 pt-2">
            <ul class="list-reset flex justify-end text-xl">
                @guest
                    <li class="mr-6">
                        <a class="text-white hover:text-grey-light no-underline" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>

                    <li class="mr-6">
                        <a class="text-white hover:text-grey-light no-underline" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @else
                    <li class="mr-6">
                        <a class="text-white hover:text-grey-light no-underline" href="{{ route('board.index') }}">Boards</a>
                    </li>

                    <li class="mr-6">
                        <div>
                            <a class="text-white hover:text-grey-light no-underline" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>

    </div>
</nav>