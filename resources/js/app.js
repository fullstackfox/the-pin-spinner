require('./bootstrap');

window.Vue = require('vue');


Vue.component('new-board', require('./components/NewBoard.vue'));
Vue.component('boards', require('./components/Boards.vue'));
Vue.component('board', require('./components/Board.vue'));

const app = new Vue({
    el: '#app'
});
